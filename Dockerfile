FROM openjdk:8-jre
VOLUME /tmp
WORKDIR /data
COPY target/greeting-app-0.0.1-SNAPSHOT.jar  /data
EXPOSE 8080
ENTRYPOINT ["java" ,"-jar", "greeting-app-0.0.1-SNAPSHOT.jar"]