package com.demo.greeting.app.interceptor;


import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.demo.greeting.app.logging.service.LoggingServiceImpl;

@ControllerAdvice
@Order(2)
public class LoggingInterceptor implements HandlerInterceptor, ResponseBodyAdvice<Object> {

	@Autowired
	LoggingServiceImpl loggingService;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		if (DispatcherType.REQUEST.name().equals(request.getDispatcherType().name())
				&& request.getMethod().equals(HttpMethod.GET.name())) {
			loggingService.logRequest(request, null);
		}
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object responseBody, MethodParameter methodParameter, MediaType mediaType,
			Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest,
			ServerHttpResponse serverHttpResponse) {
		if (serverHttpRequest instanceof ServletServerHttpRequest
				&& serverHttpResponse instanceof ServletServerHttpResponse) {
			loggingService.logResponse(serverHttpRequest, serverHttpResponse, responseBody);
		}
		return responseBody;
	}

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

}
