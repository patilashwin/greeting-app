package com.demo.greeting.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class GreetingController {
	
	@GetMapping("/{name}")
	public String greet(@PathVariable("name") String name) {
		return String.format("Hello, %s", name);
	}	
	
	@GetMapping("/status")
	public String status() {
		return "UP";
	}
}
