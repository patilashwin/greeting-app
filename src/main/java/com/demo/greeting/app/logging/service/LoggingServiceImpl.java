package com.demo.greeting.app.logging.service;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class LoggingServiceImpl {

	@Autowired
	ObjectWriter objectWriter;

	public void logRequest(HttpServletRequest httpServletRequest, Object body) {
		final Map<String, Object> requestPayload = new HashMap<>();
		final Map<String, String> parameters = buildParametersMap(httpServletRequest);
		requestPayload.put("Method", httpServletRequest.getMethod());
		requestPayload.put("URL", httpServletRequest.getRequestURL());
		requestPayload.put("Query Parameters", parameters);
		final HashMap<?, ?> map = (HashMap<?, ?>) httpServletRequest
				.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		requestPayload.put("Path Parameters", map);
		requestPayload.put("Headers", buildHeadersMap(httpServletRequest));
		requestPayload.put("Body", body != null ? body : "");
		try {
			log.info(String.format("Request payload %s", objectWriter.writeValueAsString(requestPayload)));
		} catch (final JsonProcessingException e) {
			log.error("Failed to serialize request payload", e);
		}
	}

	public void logResponse(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, Object body) {
		final Map<String, Object> responsePayload = new HashMap<>();
		responsePayload.put("Method", serverHttpRequest.getMethod());
		responsePayload.put("URL", serverHttpRequest.getURI());
		responsePayload.put("Headers", buildHeadersMap(serverHttpResponse));
		responsePayload.put("Body", body != null ? body : "");
		try {
			log.info(String.format("Response payload %s", objectWriter.writeValueAsString(responsePayload)));
		} catch (final JsonProcessingException e) {
			log.error("Failed to serialize response payload", e);
		}
	}

	private Map<String, String> buildParametersMap(HttpServletRequest httpServletRequest) {
		final Map<String, String> resultMap = new HashMap<>();
		final Enumeration<String> parameterNames = httpServletRequest.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			final String key = parameterNames.nextElement();
			final String value = httpServletRequest.getParameter(key);
			resultMap.put(key, value);
		}
		return resultMap;
	}

	private Map<String, String> buildHeadersMap(HttpServletRequest request) {
		final Map<String, String> map = new HashMap<>();
		final Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			final String key = headerNames.nextElement();
			final String value = request.getHeader(key);
			map.put(key, value);
		}
		return map;
	}

	private Map<String, String> buildHeadersMap(ServerHttpResponse response) {
		return response.getHeaders().toSingleValueMap();
	}
}